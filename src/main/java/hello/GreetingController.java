package hello;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Created by Aleksandr on 07.01.2016.
 */
@Controller
public class GreetingController {
    @MessageMapping("/hello")
    @SendTo(value = "/topic/greetings")
    public Greeting greeting(HelloMessage message) throws Exception {
        Thread.sleep(3000); // simulated delay
        System.out.println("Получено: " + message.getName());
        return new Greeting("Hello, " + message.getName() + "!");
    }
}
