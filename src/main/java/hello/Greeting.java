package hello;

/**
 * Created by Aleksandr on 07.01.2016.
 */
public class Greeting {
    private String content;

    public Greeting(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
